CREATE SCHEMA mb_matching;

CREATE TABLE mb_matching.processed (
    id text not null primary key,
	created timestamp with time zone not null default now()
);

CREATE TABLE mb_matching.release_match (
    id text not null,
	mbid uuid not null,
	valid boolean not null default false,
	confirmed boolean not null default false,
	created timestamp with time zone not null default now(),
	changed timestamp with time zone,
	confirmed_by text,
	primary key (id, mbid)
);

CREATE TABLE mb_matching.release (
    id text not null primary key,
	data text not null,
	created timestamp with time zone not null default now()
);

