import sys
import json
import sqlite3
import urllib
import urllib2
import time
import config


def submit_fingerprint(db, id, mbid, duration, fingerprint):
    params = {
        'format': 'json',
        'client': config.JMD_ACOUSTID_CLIENT_KEY,
        'user': config.JMD_ACOUSTID_USER_KEY,
        'duration': str(duration),
        'fingerprint': fingerprint,
        'foreignid': 'jamendo:{0}'.format(id),
    }

    if mbid:
        params['mbid'] = mbid

    data = urllib.urlencode(params)
    print 'submitting', id, mbid, duration, fingerprint[:20] + '...'
    response = urllib2.urlopen('http://api.acoustid.org/v2/submit', data)
    response_content = json.loads(response.read())
    if response_content['status'] != 'ok':
        print 'submission failed', response_content
        return

    cursor = db.cursor()
    cursor.execute('UPDATE jamendo_track SET imported = 1 WHERE id = ?', (id,))
    db.commit()


def main():
    db = sqlite3.connect('jamendo.db')

    cursor = db.cursor()
    cursor.execute('SELECT id, mbid, duration, fingerprint FROM jamendo_track WHERE imported = 0')
    for id, mbid, duration, fingerprint in cursor:
        submit_fingerprint(db, id, mbid, duration, fingerprint)


if __name__ == '__main__':
    main()

