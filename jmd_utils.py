import gzip
from lxml import etree


def iter_jamendo_artists(path):
    with gzip.open(path) as file:
    	for action, elem in etree.iterparse(file, tag='artist'):
            if action == 'end' and elem.tag == 'artist':
                yield elem

            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]

