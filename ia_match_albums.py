#!/usr/bin/env python

import os
import re
import pprint
import json
import tarfile
import sys
import argparse
import logging
import logging.handlers
import psycopg2
import psycopg2.extras
import psycopg2.extensions

import config
from mb_matching import (
    AudioFile,
    group_audio_files,
    add_release,
    lookup_release,
    pformat_audio_directory,
    already_processed,
)


logger = logging.getLogger(__name__)

audio_formats = set(['vbr mp3', '24bit flac', 'flac', 'apple lossless audio', 'ogg vorbis'])


def iter_json_files_in_tar(filename, offset=0, limit=0):
    count = 0
    with tarfile.open(filename, 'r') as tar:
        for entry in tar:
            if not entry.isreg():
                continue
            if not entry.name.endswith('.json'):
                continue
            count += 1
            if count >= offset:
                yield tar.extractfile(entry)
            if limit and count >= offset + limit:
                break


def parse_ia_file(meta):
    if meta['source'] != 'original':
        return None
    if meta['format'].lower() not in audio_formats:
        return None

    logger.debug('parsing file %s', meta['name'])
    #logger.debug('json data %s', pprint.pformat(meta))

    file = AudioFile('ia:' + meta['sha1'])
    file.length = float(meta.get('length', '0'))
    if not file.length:
        logger.warning('ignoring %s because it has no length attribute', meta['name'])
        return None

    file.album = meta.get('album', '')
    found_disc_no = False
    if file.album:
        match = re.match(r'(.*?) \(disc (\d+)\)$', file.album)
        if match is not None:
            file.album = match.group(1)
            file.disc_no = int(match.group(2))
            found_disc_no = True

    if not found_disc_no:
        name = os.path.dirname(meta['name'])
        match = re.search(r'\b(?:cd|disc|disk)\s*(\d+)\b', name, re.I)
        if match is not None:
            file.disc_no = int(match.group(1))
            found_disc_no = True

    file.artist = meta.get('artist', '')

    file.title = meta.get('title', '')
    if not file.title:
        name = os.path.basename(meta['name'])
        patterns = [
            r'^\d+\.?\s*(.*?)\.(\w+)$',
            r'^\d+\s*-.*?-\(\d{4}\)-(.*?)\.(\w+)$',
            r'^\d+\s*-(.*?)\.(\w+)$',
        ]
        for pattern in patterns:
            match = re.match(pattern, name)
            if match is not None:
                file.title = match.group(1)
                break

    track = meta.get('track')
    if track:
        if '/' in track:
            track = track.split('/')[0]
        file.track_no = int(track)
    else:
        name = os.path.basename(meta['name'])
        match = re.match(r'^(\d+)', name)
        if match is not None:
            file.track_no = int(match.group(1))

    ext_ids = meta.get('external-identifier')
    if ext_ids:
        if isinstance(ext_ids, basestring):
            ext_ids = [ext_ids]
        for ext_id in ext_ids:
            parts = ext_id.split(':')
            if parts[:2] == ['urn', 'mb_recording_id']:
                file.ext_ids['mb_recording_id'] = parts[2]
            elif parts[:2] == ['urn', 'acoustid'] and parts[2] != 'unknown':
                file.ext_ids['acoustid'] = parts[2]

    return file


def parse_ia_meta_json(json_file):
    meta = json.load(json_file)
    #logger.debug('json data %s', pprint.pformat(meta))

    files = []
    for file_meta in meta['files']:
        file = parse_ia_file(file_meta)
        if file is not None:
            files.append(file)

    directory = group_audio_files('ia:' + meta['metadata']['identifier'], files)
    if directory is not None:
        ext_ids = meta['metadata'].get('external-identifier')
        if ext_ids:
            if isinstance(ext_ids, basestring):
                ext_ids = [ext_ids]
            for ext_id in ext_ids:
                parts = ext_id.split(':')
                if parts[:2] == ['urn', 'mb_release_id']:
                    directory.ext_ids['mb_release_id'] = parts[2]
                elif parts[:2] == ['urn', 'acoustid'] and parts[2] != 'unknown':
                    directory.ext_ids['acoustid'] = parts[2]

    return directory


def process_ia_meta_file(db, json_file):
    logger.info('processing %s', json_file.name)

    id = re.search('/(.*?)_meta.json', json_file.name).group(1)

    if already_processed(db, id):
        logger.info('already processed')
        return

    try:
        release = parse_ia_meta_json(json_file)
    except Exception, e:
        logger.exception('error while parsing json')
        return

    if release is None:
        return

    if 'mb_release_id' in release.ext_ids:
        logger.info('already matched (externally)')
        return

    logger.debug('data %s', pformat_audio_directory(release))

    add_release(db, release)
    lookup_release(db, release)



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    parser.add_argument('-l', '--limit', type=int, default=0)
    args = parser.parse_args()

    db = psycopg2.connect(user=config.MB_DB_USER, database=config.MB_DB_NAME, host=config.MB_DB_HOST, password=config.MB_DB_PASSWORD)
    db.set_client_encoding('UTF8')

    cursor = db.cursor()
    cursor.execute("SET search_path TO musicbrainz, public")

    psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

    formatter = logging.Formatter('%(message)s')

    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logging.getLogger().addHandler(handler)

    handler = logging.FileHandler('ia_match_albums.log')
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logging.getLogger().addHandler(handler)

    logging.getLogger().setLevel(logging.DEBUG)

    for file in iter_json_files_in_tar(args.file, limit=args.limit):
        process_ia_meta_file(db, file)
        logger.info('---')
        db.commit()


if __name__ == '__main__':
	main()

