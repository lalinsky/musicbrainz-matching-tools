import urllib2
import json
import psycopg2
import psycopg2.extras
import psycopg2.extensions
from werkzeug.urls import url_encode
from flask import Flask, render_template, request, session, redirect
from mb_matching import get_release, get_mb_release
from utils import format_time as format_time_ms


app = Flask(__name__)
app.config.from_object('config')


@app.route("/login")
def login():
    if 'code' in request.args:
        authorization_code = request.args['code']

        opener = urllib2.build_opener()

        url = 'https://musicbrainz.org/oauth2/token'
        data = url_encode({
            'grant_type': 'authorization_code',
            'code': authorization_code,
            'client_id': app.config['OAUTH_CLIENT_ID'],
            'client_secret': app.config['OAUTH_CLIENT_SECRET'],
            'redirect_uri': 'http://match.muziq.eu/login',
        })
        response = json.load(opener.open(url, data))
        access_token = response['access_token']

        url = 'https://musicbrainz.org/oauth2/userinfo'
        opener.addheaders = [('Authorization', 'Bearer ' + access_token)]
        response = json.load(opener.open(url, data))
        session['user'] = response['sub']
        return redirect('/')
    else:
        url = 'https://musicbrainz.org/oauth2/authorize?' + url_encode({
            'response_type': 'code',
            'client_id': app.config['OAUTH_CLIENT_ID'],
            'redirect_uri': 'http://match.muziq.eu/login',
            'scope': 'profile',
        })
        return redirect(url)


@app.route("/logout")
def logout():
    if 'user' in session:
        del session['user']
    return redirect('/')


@app.route("/", methods=['GET', 'POST'])
def index():
    if not session.get('user'):
        return render_template('login.html')

    db = psycopg2.connect(user=app.config['MB_DB_USER'], database=app.config['MB_DB_NAME'], host=app.config['MB_DB_HOST'], password=app.config['MB_DB_PASSWORD'])
    db.set_client_encoding('UTF8')

    cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("SET search_path TO musicbrainz, public")

    psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

    if request.method == 'POST':
        parts = request.form['action'].split()
        action = parts[0]
        if action in ('confirm', 'reject'):
            id, mbid = parts[1:]
            cursor.execute('SELECT 1 FROM mb_matching.release_match WHERE id = %s AND mbid = %s AND confirmed = true', (id, mbid))
            if cursor.fetchone() is None:
                if action == 'confirm':
                    print 'confirming', id, mbid
                    cursor.execute('UPDATE mb_matching.release_match SET valid = true, confirmed = true, changed = now(), confirmed_by = %s WHERE id = %s AND mbid = %s', (session['user'], id, mbid))
                    db.commit()
                elif action == 'reject':
                    print 'rejecting', id, mbid
                    cursor.execute('UPDATE mb_matching.release_match SET valid = false, confirmed = true, changed = now(), confirmed_by = %s WHERE id = %s AND mbid = %s', (session['user'], id, mbid))
                    db.commit()

    id = request.args.get('id')
    if not id:
        cursor.execute('select id from mb_matching.release_match group by id having max(confirmed::int)=0 order by random() limit 1')
        row = cursor.fetchone()
        if row is not None:
            id = row[0]

    matches = None
    release = None

    print 'displaying', id
    if id:
        cursor.execute('select * from mb_matching.release_match where id = %s', (id, ))
        matches = []
        for row in cursor:
            row = dict(row)
            row['mb_release'] = get_mb_release(db, row['mbid'])
            matches.append(row)
        release = get_release(db, id)

    def union_keys(d1, d2):
        if not d1:
            d1 = {}
        if not d2:
            d2 = {}
        return sorted(set(d1.keys()) | set(d2.keys()))

    def format_time(s):
        return format_time_ms(s * 1000)

    return render_template('confirm_matches.html', id=id,
        matches=matches,
        release=release,
        union_keys=union_keys,
        format_time=format_time,
        user=session['user'])


if __name__ == "__main__":
    app.run(port=8180, debug=False)

