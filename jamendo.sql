CREATE TABLE jamendo_track (
    id int primary_key,
	mbid text,
	duration int not null,
	fingerprint text not null,
	imported int not null default 0
);
