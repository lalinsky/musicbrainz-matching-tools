import sys
import gzip
import sqlite3
import subprocess
import time
from lxml import etree
from jmd_submit import submit_fingerprint


def fingerprint_file(path):
    output = subprocess.check_output(["fpcalc", path])
    data = dict(line.strip().split("=", 1) for line in output.splitlines())
    if 'FINGERPRINT' not in data:
        return None, None
    return int(data["DURATION"]), data["FINGERPRINT"]


def process_track(cursor, track):
    id = track.find('id').text
    mbid = track.find('mbgid').text
    url = 'http://api.jamendo.com/get2/stream/track/redirect/?id={0}&streamencoding=flac'.format(id)

    cursor.execute('SELECT 1 FROM jamendo_track WHERE id = ?', (id,))
    if cursor.fetchone():
        print 'skipping', id
        return

    duration, fingerprint = fingerprint_file(url)
    if not duration or not fingerprint or duration < 10:
        print 'failed to fingerprint', id
        return

    cursor.execute('INSERT INTO jamendo_track (id, mbid, duration, fingerprint) VALUES (?, ?, ?, ?)', (id, mbid, duration, fingerprint))
    print id, mbid, duration, fingerprint[:30] + '...'

    return id, mbid, duration, fingerprint


def process_artist(db, artist):
    cursor = db.cursor()
    for track in artist.findall('Albums/album/Tracks/track'):
        result = process_track(cursor, track)
        db.commit()
        if result is not None:
            submit_fingerprint(db, *result)


def main(filename):
    db = sqlite3.connect('jamendo.db')

    with gzip.open(filename) as file:
    	for action, elem in etree.iterparse(file, tag='artist'):
            if action == 'end' and elem.tag == 'artist':
                process_artist(db, elem)

            elem.clear()
            while elem.getprevious() is not None:
                del elem.getparent()[0]


main(sys.argv[1])

