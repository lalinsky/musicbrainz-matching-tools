import re
import json
import unicodedata
import logging
import psycopg2.extras
from utils import asciipunct, unaccent, format_time

logger = logging.getLogger(__name__)


class AudioFile(object):

    def __init__(self, id):
        self.id = id
        self.album = ''
        self.artist = ''
        self.title = ''
        self.length = 0
        self.track_no = 1
        self.disc_no = 1
        self.ext_ids = {}


class AudioDirectory(object):

    def __init__(self, id, discs):
        self.id = id
        self.discs = discs
        self.ext_ids = {}


def group_audio_files(id, files):
    if not files:
        return None

    discs = {}
    for file in files:
        tracks = discs.setdefault(file.disc_no, {})
        if file.track_no in tracks:
            logger.error('duplicate track %d on disc %d', file.track_no, file.disc_no)
            return None
        tracks[file.track_no] = file

    if min(discs.keys()) != 1 or max(discs.keys()) != len(discs):
        logger.error('invalid disc numbers')
        return None

    for disc_no, tracks in discs.items():
        if min(tracks.keys()) != 1 or max(tracks.keys()) != len(tracks):
            logger.error('invalid track numbers on disc %d', disc_no)
            return None

    return AudioDirectory(id, discs)


def pformat_audio_directory(directory):
    out = []

    out.append(repr(directory))
    out.append('  id = %s' % directory.id)
    for name, value in directory.ext_ids.iteritems():
        out.append('  ext id %s = %s' % (name, value))
    for disc_no, tracks in directory.discs.iteritems():
        out.append('  disc %d' % disc_no)
        for track_no, track in tracks.iteritems():
            out.append('    track %d' % track_no)
            out.append('      id = %s' % track.id)
            out.append('      title = %s' % track.title)
            out.append('      artist = %s' % track.artist)
            out.append('      album = %s' % track.album)
            out.append('      length = %s' % track.length)
            for name, value in track.ext_ids.iteritems():
                out.append('      ext id %s = %s' % (name, value))

    return '\n'.join(out)


toc_query = """
    SELECT DISTINCT
        r.gid AS release_id,
        rn.name AS release_name,
        artist_name.name AS artist_name,
        m.id AS medium_id,
        m.position AS medium_position,
        m.name AS medium_name,
        m.id AS medium_id
    FROM
        medium m
        JOIN medium_index mi ON mi.medium = m.id
        JOIN release r ON m.release = r.id
        JOIN release_name rn ON r.name = rn.id
        JOIN artist_credit ON r.artist_credit = artist_credit.id
        JOIN artist_name ON artist_credit.name = artist_name.id
    WHERE
        toc <@ create_bounding_cube(%(durations)s, %(fuzzy)s::int) AND
        track_count = %(num_tracks)s
"""


tracklist_query = """
    SELECT
        recording.gid AS mb_recording_id,
        track_name.name AS title,
        track.length AS length,
        track.position AS track_no
    FROM
        track
        JOIN track_name ON track.name = track_name.id
        JOIN recording ON track.recording = recording.id
        JOIN track_name recording_name ON recording.name = recording_name.id
    WHERE
        track.medium = %(medium)s
    ORDER BY
        track.position
"""


mediums_query = """
    SELECT id, position FROM medium WHERE release = (SELECT id FROM release WHERE gid = %(release)s) ORDER BY position
"""


medium_count_query = """
    SELECT count(*) FROM medium WHERE release = (SELECT id FROM release WHERE gid = %(release)s)
"""


def get_mb_release(db, id):
    cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(mediums_query, dict(release=id))

    release = AudioDirectory('mbid:' + id, {})
    for row in cursor:
        disc_no = row['position']
        tracks = get_mb_tracks(db, row['id'])
        for track in tracks.itervalues():
            track.disc_no = disc_no
        release.discs[disc_no] = tracks

    return release


def get_mb_tracks(db, id):
    cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(tracklist_query, dict(medium=id))

    tracks = {}
    for row in cursor:
        track = AudioFile('mbid:' + row['mb_recording_id'])
        track.track_no = row['track_no']
        track.title = row['title']
        track.length = (row['length'] or 0) / 1000.0
        tracks[track.track_no] = track
    return tracks


def get_medium_count(db, id):
    cursor = db.cursor()
    cursor.execute(medium_count_query, dict(release=id))
    row = cursor.fetchone()
    return row[0]


def lookup_disc(db, tracks):
    durations = [int(t.length * 1000) for (t_no, t) in sorted(tracks.items())]
    toc = ' '.join(map(format_time, durations))
    logger.info('looking up TOC %s', toc)
    if any(d >= 2 ** 32 for d in durations):
        logger.warning('invalid TOC')
        return []
    if len(durations) <= 5:
        logger.warning('too few tracks')
        return []
    else:
        cursor = db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute(toc_query, dict(durations=durations, fuzzy=5000, num_tracks=len(durations)))
        return cursor.fetchall()


def lookup_release(db, release):
    disc_matches = {}
    common_releases = {}
    all_releases = set()
    for disc_no, tracks in release.discs.items():
        matches = lookup_disc(db, tracks)
        if not matches:
            logger.info('no matching releases')
        else:
            disc_matches[disc_no] = matches
            for match in matches:
                all_releases.add(match['release_id'])
                common_releases.setdefault(match['release_id'], []).append((disc_no, match))
                logger.info('found %r', match)

    submit = set()
    releases = set()
    file_mbids = {}
    for release_id, disc_nos in common_releases.items():
        if len(disc_nos) != len(release.discs):
            logger.info('release %s matched only %d out of %d discs', release_id, len(disc_nos), len(release.discs))
            continue

        medium_count = get_medium_count(db, release_id)
        if medium_count != len(release.discs):
            logger.info('directory has only %d out of %d discs on the release', len(release.discs), medium_count)
            continue

        logger.info('delease %s matched all discs', release_id)

        mb_discs = {}
        scores = {}
        for disc_no, match in sorted(disc_nos):
            logger.info('evaluating %r as disc %d', match, disc_no)
            tracks = release.discs[disc_no]
            mb_tracks = get_mb_tracks(db, match['medium_id'])
            mb_discs[disc_no] = mb_tracks
            if mb_tracks.keys() != tracks.keys():
                logger.info('track numbers do not match %r vs %r', mb_tracks.keys(), tracks.keys())
                continue
            track_scores = []
            for track_no in tracks.keys():
                track = tracks[track_no]
                mb_track = mb_tracks[track_no]
                score = compare_names(mb_track, track)
                logger.debug('track %d %s with names %r and %r = %0.2f', track_no, '**' if score <= 0.3 else '  ', mb_track.title, track.title, score)
                track_scores.append(score)
            if min(track_scores) > 0.3:
                scores[disc_no] = sum(track_scores)
            elif min(track_scores) > 0.1:
                #sqlite_db.execute('INSERT INTO ask_later (bucket) VALUES (?)', (os.path.basename(bucket_dir),))
                #print "maybe new release", re, release_id
                #if raw_input('Score too low, match anyway? ').lower() == 'y':
                #    scores[disc_no] = sum(track_scores)
                pass

        if len(scores) == len(release.discs):
            logger.info('match!')
            tracks = release.discs[disc_no]
            mb_tracks = mb_discs[disc_no]
            #for track_no in tracks.keys():
            #    track = tracks[track_no]
            #    if track['sha1'] not in file_mbids:
            #        file_mbids[track['sha1']] = []
            #    mb_track = mb_tracks[track_no]
            #    file_mbids[track['sha1']].append({
            #        'mb_release_id': release_id,
            #        'mb_recording_id': mb_track['mb_recording_id'],
            #        'acoustid': track.get('acoustid')
            #    })
            releases.add(release_id)

    for release_id in all_releases:
        add_release_match(db, release.id, release_id, release_id in releases)


def already_processed(db, id):
    cursor = db.cursor()
    cursor.execute("SELECT 1 FROM mb_matching.processed WHERE id = %s", (id, ))
    if cursor.fetchone() is not None:
        return True

    cursor.execute("INSERT INTO mb_matching.processed (id) VALUES (%s)", (id, ))
    return False


def add_release(db, directory):
    data = dict(directory.__dict__)
    data['discs'] = {}
    for disc_no, tracks in directory.discs.iteritems():
        data_tracks = data['discs'].setdefault(disc_no, {})
        for track_no, track in tracks.iteritems():
            data_tracks[track_no] = dict(track.__dict__)

    cursor = db.cursor()
    cursor.execute("INSERT INTO mb_matching.release (id, data) VALUES (%s, %s)", (directory.id, json.dumps(data)))


def get_release(db, id):
    cursor = db.cursor()
    cursor.execute('SELECT data FROM mb_matching.release WHERE id = %s', (id, ))
    data = json.loads(cursor.fetchone()[0])

    release = AudioDirectory(id, {})
    for name, value in data.iteritems():
        if name == 'discs':
            continue
        setattr(release, name, value)

    for disc_no, tracks in data['discs'].iteritems():
        disc_no = int(disc_no)
        release.discs[disc_no] = {}
        for track_no, track in tracks.iteritems():
            track_no = int(track_no)
            t = AudioFile(track['id'])
            for name, value in track.iteritems():
                setattr(t, name, value)
            release.discs[disc_no][track_no] = t

    return release


def add_release_match(db, id, mbid, confirmed=False):
    cursor = db.cursor()
    cursor.execute("INSERT INTO mb_matching.release_match (id, mbid, confirmed, valid) VALUES (%s, %s, %s, true)", (id, mbid, confirmed))


def normalize_name(name):
    name = unicode(name)
    name = name.lower()
    name = unicodedata.normalize('NFKC', name)
    name = unaccent(name)
    name = asciipunct(name)
    return name


def remove_extra_title_information(name):
    name = re.sub(r'\((?:feat\.?|ft\.?|featuring|theme) [^)]+\)', '', name, re.U | re.I)
    name = re.sub(r'\[(?:feat\.?|ft\.?|featuring|theme) [^)]+\]', '', name, re.U | re.I)
    name = re.sub(r'\(bonus( track)?\)', '', name, re.U | re.I)
    name = re.sub(r'\s+\([^)]+\)$', '', name, re.U | re.I)
    return name


def extract_words(orig_name):
    name = ''.join(re.findall(r'\w+', orig_name, re.U))
    words = []
    for i in range(0, len(name) - 1):
        words.append(name[i:i+2])
    if not words:
        words.append(orig_name)
    return words


def iter_normalized_names(meta_a, meta_b):
    name_a = normalize_name(meta_a.title)
    name_b = normalize_name(meta_b.title)
    yield [name_a], [name_b]
    yield extract_words(name_a), extract_words(name_b)
    # remove extra title information from both names
    name_a = remove_extra_title_information(name_a)
    name_b = remove_extra_title_information(name_b)
    yield extract_words(name_a), extract_words(name_b)
    # remove artist name from the IA metadata
    artist = normalize_name(meta_b.artist)
    if artist in name_b:
        x_name_b = name_b.replace(artist, '')
        yield extract_words(name_a), extract_words(x_name_b)
    if ' - ' in name_b:
        x_name_b = name_b.split(' - ', 1)[1]
        yield extract_words(name_a), extract_words(x_name_b)
    if ' / ' in name_b:
        x_name_b = name_b.split(' / ', 1)[1]
        yield extract_words(name_a), extract_words(x_name_b)


def compare_word_sets(words_a, words_b):
    word_set_a = {}
    for word in words_a:
        if word not in word_set_a:
            word_set_a[word] = 0
        word_set_a[word] += 1
    word_set_b = {}
    for word in words_b:
        if word not in word_set_b:
            word_set_b[word] = 0
        word_set_b[word] += 1
    common_words = 0
    all_words = 0
    for word in set(words_a) | set(words_b):
        count_a = word_set_a.get(word, 0)
        count_b = word_set_b.get(word, 0)
        while count_a > 0 and count_b > 0:
            all_words += 1
            common_words += 1
            count_a -= 1
            count_b -= 1
        all_words += count_a
        all_words += count_b
    score = 1.0 * common_words / all_words
    if score < 0.5:
        min_count = min(sum(word_set_a.values()), sum(word_set_b.values()))
        new_score = 1.0 * common_words / min_count
        if min_count > 6 and new_score >= 1.0:
            score = new_score
    return score


def compare_names(meta_a, meta_b):
    max_score = 0
    for a, b in iter_normalized_names(meta_a, meta_b):
        score = compare_word_sets(a, b)
        if score >= 1:
            return score
        max_score = max(score, max_score)
    return max_score

