Music Matching Tools
====================

Setup:

    cp config.py.dist config.py
    vim confing.py

Jamendo
-------

Download and fingerprint Jamendo tracks:

    wget http://imgjam.com/data/dbdump_artistalbumtrack.xml.gz
    sqlite3 jamendo.db <jamendo.sql
    python jmd_import.py dbdump_artistalbumtrack.xml.gz

Report with Jamendo-MBID matches:

    SELECT id FROM foreign_vendor WHERE name = 'jamendo';

    SELECT
        f.name AS jamendo_track_id,
        t.gid AS acoustid,
        mbid
    FROM
        foreignid f
        JOIN track_foreignid tf ON f.id = tf.foreignid_id
        JOIN track_mbid tm ON tf.track_id = tm.track_id
		JOIN track t ON tf.track_id = t.id
    WHERE vendor_id = 2
    ORDER BY f.name, t.gid, mbid;

