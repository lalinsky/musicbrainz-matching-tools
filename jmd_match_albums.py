import sys
import logging
import time
import argparse
import psycopg2
import psycopg2.extras
import psycopg2.extensions

import config
from mb_matching import (
    AudioFile,
    group_audio_files,
    add_release,
    lookup_release,
    pformat_audio_directory,
    already_processed,
)
from jmd_utils import iter_jamendo_artists


logger = logging.getLogger(__name__)


def iter_jamendo_releases(path):
    for artist_el in iter_jamendo_artists(path):
        artist_name = artist_el.find('name').text
        for album_el in artist_el.findall('Albums/album'):
            tracks = []
            for track_el in album_el.findall('Tracks/track'):
                track = AudioFile('jamendo:track:' + track_el.find('id').text)
                track.title = track_el.find('name').text
                track.length = float(track_el.find('duration').text)
                track.track_no = int(track_el.find('numalbum').text)
                tracks.append(track)
            release = group_audio_files('jamendo:album:' + album_el.find('id').text, tracks)
            if release is not None:
                yield release


def process_jamendo_release(db, release):
    logger.info('processing %s', release.id)

    if already_processed(db, release.id):
        logger.info('already processed')
        return

    logger.debug('data %s', pformat_audio_directory(release))

    add_release(db, release)
    lookup_release(db, release)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    args = parser.parse_args()

    db = psycopg2.connect(user=config.MB_DB_USER, database=config.MB_DB_NAME, host=config.MB_DB_HOST, password=config.MB_DB_PASSWORD)
    db.set_client_encoding('UTF8')

    cursor = db.cursor()
    cursor.execute("SET search_path TO musicbrainz, public")

    psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

    formatter = logging.Formatter('%(message)s')

    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logging.getLogger().addHandler(handler)

    handler = logging.FileHandler('jmd_match_albums.log')
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)
    logging.getLogger().addHandler(handler)

    logging.getLogger().setLevel(logging.DEBUG)

    for release in iter_jamendo_releases(args.file):
        process_jamendo_release(db, release)
        logger.info('---')
        db.commit()


if __name__ == '__main__':
    main()

